.PHONY: test

deps:
	pip install -r requirements_test.txt

test:
	pytest -v tests

lint:
	flake8 basics tests