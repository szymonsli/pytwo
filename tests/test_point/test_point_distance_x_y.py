from basics.point import Point
import pytest


class TestPointDistanceXOrY:
    @pytest.mark.parametrize("x1, x2, dx", [
        (3.4, 1.3, 2.1),
        (-1.43422, 345.86343, 347.297),
        (0, 0, 0)
    ])
    def test_point_distance_x_correct(self, x1, x2, dx):
        p1 = Point(x1, -2.7)
        p2 = Point(x2, 0.3)
        distance_x = p1.distance_x(p2)
        assert distance_x == dx

    @pytest.mark.parametrize("value", [
        13,
        "hello",
        True,
        False,
        [3, 4, 5],
        {"a": 4}
    ])
    def test_point_distance_x_incorrect_point(self, value):
        p1 = Point(2.5, -2.7)
        distance_x = p1.distance_x(value)
        assert distance_x == 0

    @pytest.mark.parametrize("y1, y2, dy", [
        (7.5, 8.67, 1.17),
        (565.8756, -54.71232, 620.588),
        (0, 0, 0)
    ])
    def test_point_distance_y_correct(self, y1, y2, dy):
        p1 = Point(2.5, y1)
        p2 = Point(-1.5, y2)
        distance_y = p1.distance_y(p2)
        assert distance_y == dy

    @pytest.mark.parametrize("value", [
        13,
        "hello",
        True,
        False,
        [3, 4, 5],
        {"a": 4}
    ])
    def test_point_distance_y_incorrect_point(self, value):
        p1 = Point(2.5, -2.7)
        distance_y = p1.distance_y(value)
        assert distance_y == 0
