import pytest
from basics.point import Point


class TestPoint:
    @pytest.mark.parametrize("x, y, expected_x, expected_y", [
        (2, 3, 2.0, 3.0),
        (132.546, -3.78, 132.546, -3.78),
        (34.564356, -12.45655, 34.564, -12.457)

    ])
    def test_point_correct(self, x, y, expected_x, expected_y):
        p = Point(x, y)
        assert p.x == expected_x
        assert p.y == expected_y

    @pytest.mark.parametrize("x, y", [
        ([5, 6, 4],  [5.6, "abc", [2, 3]]),
        ("hello", "string"),
        (True, True),
        (False, False),
        ({"c": [2, 3], "d": 23.6}, {"a": 9, "b": "hello"})
    ])
    def test_point_incorrect(self, x, y):
        p = Point(x, y)
        assert p.x == 0.0
        assert p.y == 0.0
