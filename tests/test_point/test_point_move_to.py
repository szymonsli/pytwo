import pytest
from basics.point import Point


class TestPointMoveTo:
    @pytest.mark.parametrize("x, y", [
        (4, 1.8),
        (0, 0),
        (-4, -2.3),
        (34.12351, 17.7826)
    ])
    def test_point_move_to_correct(self, x, y):
        p = Point(3.5, -2.7)
        p.move_to(x, y)
        assert p.x == round(x, 3)
        assert p.y == round(y, 3)

    @pytest.mark.parametrize("x, y", [
        ("hello", "string"),
        ([3.4, 5], ["abc", [2, 3]]),
        ((4, 6), (5.6, "abc")),
        ({"a": 5}, {}),
        (True, True),
        (False, False)
    ])
    def test_point_move_to_incorrect(self, x, y):
        p = Point(3.5, -2.7)
        p.move_to(x, y)
        assert p.x == 3.5
        assert p.y == -2.7
