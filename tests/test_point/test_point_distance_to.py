from basics.point import Point
import pytest


class TestPointDistanceTo:
    def test_point_distance_to_point_correct(self):
        p1 = Point(2.5, -2.7)
        p2 = Point(-1.5, 0.3)
        distance = p1.distance_to_point(p2)
        assert distance == 5

    @pytest.mark.parametrize("value", [
        "hello",
        True,
        False,
        [3, 4, 5],
        {"a": 4}
    ])
    def test_point_distance_to_point_incorrect(self, value):
        p1 = Point(2.5, -2.7)
        distance = p1.distance_to_point(value)
        assert distance == 0
