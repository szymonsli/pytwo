import pytest
from basics.point import Point


class TestPointMoveBy:
    @pytest.mark.parametrize("x, y, expected_x, expected_y", [
        (4.0, 1.8, 7.5, -0.9),
        (0, 0, 3.5, -2.7),
        (-4, -2.3, -0.5, -5.0),
        (34.12351, 17.7826, 37.624, 15.083)
    ])
    def test_point_move_by_correct(self, x, y, expected_x, expected_y):
        p = Point(3.5, -2.7)
        p.move_by(x, y)
        # additional rounding because of Python's float issue
        assert round(p.x, 7) == expected_x
        assert round(p.y, 7) == expected_y

    @pytest.mark.parametrize("x, y", [
        ("hello", "string"),
        ([3.4, 5], ["abc", [2, 3]]),
        ((4, 6), (5.6, "abc")),
        ({"a": 5}, {}),
        (True, True),
        (False, False)
    ])
    def test_point_move_by_incorrect(self, x, y):
        p = Point(3.5, -2.7)
        p.move_by(x, y)
        assert p.x == 3.5
        assert p.y == -2.7
