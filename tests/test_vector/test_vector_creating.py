import pytest
from math import sqrt
from basics.vector import Vector
from basics.point import Point


class TestVectorCreating:
    @pytest.mark.parametrize("x1, y1, x2, y2", [
        (3.4, 7.5, 1.3, 8.67),
        (-1.43422, 565.8756, 345.86343, -54.71232),
        (0, 0, 0, 0)
    ])
    def test_vector_creating_correct(self, x1, y1, x2, y2):
        p1 = Point(x1, y1)
        p2 = Point(x2, y2)
        v1 = Vector(p1, p2)
        assert v1.x == round(x2, 3) - round(x1, 3)
        assert v1.y == round(y2, 3) - round(y1, 3)
        assert v1.value == round(sqrt(v1.x**2 + v1.y**2), 3)

    @pytest.mark.parametrize("p1, p2", [
        ("text", "str"),
        ([-1.43422, "a"], [4, 6, 4]),
        (True, True),
        (False, False),
    ])
    def test_vector_creating_incorrect(self, p1, p2):
        v1 = Vector(p1, p2)
        assert v1.x == 0
        assert v1.y == 0
        assert v1.value == 0
