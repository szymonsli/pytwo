from .point import Point


class Vector:
    def __init__(self, point_start: Point, point_end: Point) -> None:
        try:
            self.x = point_end.x - point_start.x
            self.y = point_end.y - point_start.y
            self.value = point_start.distance_to_point(point_end)
        except AttributeError:
            print('''
Error:
Function arguments should be instances of Point class.
Assigned 0 to vector's x, y and value attributes.''')
            self.x = 0
            self.y = 0
            self.value = 0
