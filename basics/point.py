from math import sqrt


class Point:
    def __init__(self, x: float, y: float):
        self.x = round(x if type(x) is float or type(x) is int else 0.0, 3)
        self.y = round(y if type(y) is float or type(y) is int else 0.0, 3)

    def move_by(self, by_x: float, by_y: float) -> None:
        if type(by_x) is float or type(by_x) is int:
            self.x += round(by_x, 3)
        if type(by_y) is float or type(by_y) is int:
            self.y += round(by_y, 3)

    def move_to(self, to_x: float, to_y: float) -> None:
        if type(to_x) is float or type(to_x) is int:
            self.x = round(to_x, 3)
        if type(to_y) is float or type(to_y) is int:
            self.y = round(to_y, 3)

    def distance_x(self, point: 'Point') -> float:
        x1 = self.x
        if isinstance(point, Point):
            x2 = point.x
            return round(abs(x2 - x1), 3)
        else:
            return 0

    def distance_y(self, point: 'Point') -> float:
        y1 = self.y
        if isinstance(point, Point):
            y2 = point.y
            return round(abs(y2 - y1), 3)
        else:
            return 0

    def distance_to_point(self, point: 'Point') -> float:
        if isinstance(point, Point):
            dx = self.distance_x(point)
            dy = self.distance_y(point)
            return round(sqrt(dx**2 + dy**2), 3)
        else:
            return 0
